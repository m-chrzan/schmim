module Main where

import System.IO ( stdin, hGetContents )
import System.Environment ( getArgs, getProgName )
import System.Exit ( exitFailure, exitSuccess )

import Parser.LexSchmim
import Parser.ParSchmim
import Parser.SkelSchmim
import Parser.PrintSchmim
import Parser.AbsSchmim

import Eval

import Parser.ErrM

type ParseFun a = [Token] -> Err a

myLLexer = myLexer

type Verbosity = Int

putStrV :: Verbosity -> String -> IO ()
putStrV v s = if v > 1 then putStrLn s else return ()

runFile :: Verbosity -> ParseFun Exp -> FilePath -> IO ()
runFile v p f = readFile f >>= run v p

run :: Verbosity -> ParseFun Exp -> String -> IO ()
run v p s = let ts = myLLexer s in case p ts of
           Bad s    -> do putStrLn "Syntax error"
                          exitFailure
           Ok  tree -> do let res = interp tree
                          case res of
                            (Left err) -> do putStrLn $ "Error: " ++ err
                                             exitFailure
                            (Right v) -> print v

                          exitSuccess

usage :: IO ()
usage = do
  putStrLn $ unlines
    [ "usage: Call with one of the following argument combinations:"
    , "  --help          Display this help message."
    , "  (no arguments)  Parse stdin verbosely."
    , "  (files)         Parse content of files verbosely."
    , "  -s (files)      Silent mode. Parse content of files silently."
    ]
  exitFailure

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["--help"] -> usage
    [] -> hGetContents stdin >>= run 2 pExp
    "-s":fs -> mapM_ (runFile 0 pExp) fs
    fs -> mapM_ (runFile 2 pExp) fs
