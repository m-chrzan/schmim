module Abs where

import Parser.AbsSchmim
import Data.Map.Lazy

type Env = Map Ident Val

data Val
    = VNum Integer
    | VAbs Ident Type Env Exp
    | VTr
    | VFl
    | VVrnt Ident Val Type
    | VTpl [Val]
    | VNil Type
    | VCons Val Val
  deriving (Eq, Ord, Read)

instance Show Val where
    show (VNum n) = show n
    show (VAbs x t _ _) = "#(lambda " ++ (show x) ++ " : " ++ (show t) ++ ")"
    show (VTr) = "true"
    show (VFl) = "false"
    show (VVrnt (Ident l) v _) = "<" ++ l ++ " " ++ (show v) ++ ">"
    show (VTpl vs) = "{" ++ (show' vs) ++ "}"
    show (VNil t) = "nil"
    show (VCons v1 v2) = "(cons " ++ (show v1) ++ " " ++ (show v2) ++ ")"

show' = cleanup . Prelude.foldl (\s v -> s ++ (show v) ++ ", ") ""
    where cleanup [] = ""
          cleanup l = init $ init l
