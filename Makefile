all: bnfc parser interpreter

bnfc:
	bnfc -haskell -p Parser schmim.bnfc

parser: bnfc
	happy -gca Parser/ParSchmim.y
	alex -g Parser/LexSchmim.x

interpreter: parser bnfc
	ghc -dynamic Schmim.hs -o interpreter

clean:
	-rm -rf Parser *.o *.hi interpreter
